# -*- coding: utf-8 -*-

import json
from MapReduce import MapReduce


def map(item):
    pass


def reduce(key, values):
    pass


if __name__ == '__main__':
    #input_data = ...
    mapper = MapReduce(map, reduce)
    results = mapper(input_data)
    for row in results:
        print json.dumps(row)
